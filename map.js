var cdrcUrl = "https://maps.cdrc.ac.uk/tiles/shine_urbanmask_dark2/{z}/{x}/{y}.png",
    cdrc = L.tileLayer(cdrcUrl,
		       {"attribution": "toner-bcg", "maxNativeZoom": 18,
			"maxZoom": 14, "minZoom": 0, "noWrap": false,
			"opacity": 0.32}
		      );

var imd2010Url = "https://cdrc-maps.liv.ac.uk/tiles/imd2010_eng/{z}/{x}/{y}.png",
    imd2010 = L.tileLayer(imd2010Url, {maxZoom: 14})

var imd2015Url = "https://cdrc-maps.liv.ac.uk/tiles/imd2015_eng/{z}/{x}/{y}.png",
    imd2015 = L.tileLayer(imd2015Url, {maxZoom: 14})


var imd2019Url = "https://maps.cdrc.ac.uk/tiles/imd2019_eng/{z}/{x}/{y}.png",
    imd2019 = L.tileLayer(imd2019Url, {maxZoom: 14})

// initialize the map on the "map" div with a given center and zoom
var map = L.map('map').setView([53.3951, -1.477], 13).addLayer(imd2019).addLayer(cdrc);

L.Control.geocoder().addTo(map);


//var url_placenames="https://opendata.arcgis.com/datasets/a6c138d17ac54532b0ca8ee693922f10_0.geojson";


function filterCities(feature){
    var zoom = map.getZoom();
    var level = feature.properties["t"];
    if(level<4){
	var bounds=map.getBounds();
	var north=bounds.getNorth();
	var south=bounds.getSouth();
	var east=bounds.getEast();
	var west=bounds.getWest();

	var [lat,long]=feature.geometry.coordinates;
	if( lat<east && lat>west && long>south && long<north){
	    return true;
	}
	console.log(feature.properties["n"]);
	console.log(lat,east,west);
	return false;
	
    }
    else {return false;}
    /*}
    else{
	return false;
    }*/
};

function pointToLayer(feature, latlng) {
    //var marker = new L.marker(latlng, { opacity: 0.01 }); //opacity may be set to zero
    //marker.bindTooltip(feature.properties.n, {permanent: true, className: "my-city-label", offset: [0, 0] });

    var marker = new L.Marker(latlng, {
	icon: new L.DivIcon({
            className: 'my-city-label',
            html: '<span class="my-city-label">'+feature.properties.n+'</span>'
	})});
    
    return marker;
}

var url_placeNames="data/os_open_names_optimised.json";
//url_placeNames="data/temp.json";
var placeNames = new L.GeoJSON.AJAX(url_placeNames,{filter:filterCities,pointToLayer:pointToLayer});       
placeNames.addTo(map);

// var boroughs = L.geoJson(null, {
//     style: function(feature) {
//         return {
//             color: "black",
//             fill: false,
//             opacity: 1,
//             clickable: false
//         };
//     }// ,
//     // onEachFeature: function(feature, layer) {
//     //     boroughSearch.push({
//     //         name: layer.feature.properties.BoroName,
//     //         source: "Boroughs",
//     //         id: L.stamp(layer),
//     //         bounds: layer.getBounds()
//     //     });
//     // }
// });

// $.getJSON(path_placenames, function(data) {
//     boroughs.addData(data);
// });
//boroughs.addTo(map);

// Add the style to your layer


var constitBound = new L.GeoJSON.AJAX("data/constituencies.geojson",{
    style:{
	"color": "purple",
	"fillColor":"#ffff00",
	"weight": 5,
	"dashArray": "5, 5",
	"fillOpacity": 0.01
    },
    onEachFeature: function(feature,layer){
	layer.on({
            click: function(e) {
		map.fitBounds(e.target.getBounds());
            }
	})}
});

constitBound.bindTooltip(function (layer) {
    // Convert non-primitive to String.
    let handleObject = (feature)=>typeof(feature)=='object' ? JSON.stringify(feature) : feature;
    let fields = ["PCON13NM"];
    return '<table>' +
        String(
            fields.map(
                columnname=>
                    `<tr style="text-align: left;">
                    <td style="padding: 4px;">${handleObject(layer.feature.properties[columnname])
                    }</td></tr>`
            ).join(''))
        +'</table>'
}, {"sticky": true});

constitBound.addTo(map);


var baseMaps = {
    "imd2010": imd2010,
    "imd2015": imd2015,
    "imd2019": imd2019
};
 
var overlayMaps = {
    "Streets": cdrc,
    "Constituencies": constitBound
};
L.control.layers(baseMaps, overlayMaps).addTo(map);
 

// function onMapClick(e) {
    
//     var marker = L.marker(e.latlng, {
// 	draggable: true,
// 	title: "Resource location",
// 	alt: "Resource Location",
// 	riseOnHover: true
//     }).addTo(map)
// 	.bindPopup(e.latlng.toString()).openPopup();
    
//     // Update marker on changing it's position
//     marker.on("dragend", function (ev) {
	
// 	var chagedPos = ev.target.getLatLng();
// 	this.bindPopup(chagedPos.toString()).openPopup();
	
//     });
// }

// map.on('click', onMapClick);
